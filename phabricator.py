import configparser
import requests
from constants import *


class Phabricator:
    def __init__(self, api_token, api_basepath):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.API_TOKEN = api_token
        self.API_BASEPATH = api_basepath
        self.HEADER = {"Content-Type": "application/x-www-form-urlencoded"}

    # Get information from a ticket
    def get_ticket_info(self, ticket_id):
        data = {"api.token": self.API_TOKEN, "task_id": ticket_id}
        response = requests.post(self.API_BASEPATH + "/maniphest.info", data=data, headers=self.HEADER)
        ticket_info = response.json()
        information = {
            TICKET_ID: ticket_id,
            TICKET_OWNER: self.get_username_by_author_phid(ticket_info["result"]["ownerPHID"]),
            TICKET_TITLE: ticket_info["result"]["title"],
            TICKET_DESCRIPTION: ticket_info["result"]["description"],
            TICKET_STATUS_NAME: ticket_info["result"]["statusName"],
            TICKET_LINK: "<" + ticket_info["result"]["uri"] + "|T" + ticket_id + ">"
        }
        return information

    # Get the title/name from a ticket
    def get_ticket_title(self, ticket_id):
        response_data = self.get_ticket_info(ticket_id)
        return response_data[TICKET_TITLE]

    # Get the description from a ticket
    def get_ticket_description(self, ticket_id):
        response_data = self.get_ticket_info(ticket_id)
        return response_data[TICKET_DESCRIPTION]

    # Returns all comments (sorted by date) from a ticket
    def get_ticket_comments(self, ticket_id):
        data = {"api.token": self.API_TOKEN, "objectIdentifier": "T" + ticket_id}
        response = requests.post(self.API_BASEPATH + "/transaction.search", data=data, headers=self.HEADER)
        response_data = response.json()
        if response_data["result"] is None:
            return None
        # comments = [transaction["comments"] for transaction in response_data["result"]["data"]]
        ticket_transactions = response_data["result"]["data"]
        ticket_transactions.sort(key=lambda item: item["dateModified"])
        raw_comments = []
        for transaction in ticket_transactions:
            if len(transaction["comments"]) > 0:
                for comment in transaction["comments"]:
                    comment_information = {
                        ":construction_worker:": self.get_username_by_author_phid(comment["authorPHID"]),
                        ":page_facing_up:": comment["content"]["raw"]
                    }
                    raw_comments.append(comment_information)

        return raw_comments

    # Add a comment to a ticket
    def add_comment_to_ticket(self, ticket_id, comment):
        data = {"api.token": self.API_TOKEN, "objectIdentifier": "T" + ticket_id, "transactions[0][type]": "comment",
                "transactions[0][value]": comment}
        response = requests.post(self.API_BASEPATH + "/maniphest.edit", data=data, headers=self.HEADER)

    # FIXME size limit = 10 (slack messages limit size is 50)
    # Get all the ticket from an user
    def get_assigned_tickets_by_username(self, username):
        data = {"api.token": self.API_TOKEN, "constraints[assigned][0]": username, "limit": "20", "order": "newest"}
        response = requests.post(self.API_BASEPATH + "/maniphest.search", data=data, headers=self.HEADER)
        response_data = response.json()
        return response_data["result"]["data"]

    # Get all ticket from a user for the given status
    def get_assigned_tickets_by_username_and_status(self, username, status):
        data = {"api.token": self.API_TOKEN, "constraints[assigned][0]": username,
                "constraints[statuses][0]": status, "limit": "20", "order": "newest"}
        response = requests.post(self.API_BASEPATH + "/maniphest.search", data=data, headers=self.HEADER)
        response_data = response.json()
        return response_data["result"]["data"]

    # Get the username from a given author_phid
    def get_username_by_author_phid(self, author_phid):
        data = {"api.token": self.API_TOKEN, "constraints[phids][0]": author_phid}
        response = requests.post(self.API_BASEPATH + "/user.search", data=data, headers=self.HEADER)
        response_data = response.json()
        return response_data["result"]["data"][0]["fields"]["username"]

    # Get the current username
    def get_current_username(self):
        data = {"api.token": self.API_TOKEN}
        response = requests.post(self.API_BASEPATH + "/user.whoami", data=data, headers=self.HEADER)
        response_data = response.json()
        return response_data["result"]["userName"]

    # Get the current status from a ticket
    def get_ticket_status(self, ticket_id):
        ticket_info = self.get_ticket_info(ticket_id)
        return ticket_info[TICKET_STATUS_NAME]

    # Change the current status of a ticket
    def change_ticket_status(self, ticket_id, new_status):
        data = {"api.token": self.API_TOKEN, "objectIdentifier": "T" + ticket_id, "transactions[0][type]": "status",
                "transactions[0][value]": new_status}
        response = requests.post(self.API_BASEPATH + "/maniphest.edit", data=data, headers=self.HEADER)
        # TODO Check for errors

    # Change the current status from a ticket to open
    def open_ticket(self, ticket_id):
        self.change_ticket_status(ticket_id, OPEN_STATUS)

    def progress_ticket(self, ticket_id):
        self.change_ticket_status(ticket_id, IN_PROGRESS_STATUS)

    # Change the current status from a ticket to close
    def close_ticket(self, ticket_id):
        self.change_ticket_status(ticket_id, CLOSE_STATUS)
        pass

    # Change the current status from a ticket to invalid
    def invalidate_ticket(self, ticket_id):
        self.change_ticket_status(ticket_id, INVALID_STATUS)
        pass

    # Create a new task
    def create_task(self, title, description):
        data = {"api.token": self.API_TOKEN, "transactions[0][type]": "title",
                "transactions[0][value]": title, "transactions[1][type]": "description",
                "transactions[1][value]": description}
        response = requests.post(self.API_BASEPATH + "/maniphest.edit", data=data, headers=self.HEADER)
        return response["result"]["object"]["id"]
        # TODO Check for errors
