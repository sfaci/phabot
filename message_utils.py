# Returns a block with all the information about the tickets from an user
def get_user_tickets_message(username, tickets):
    blocks = [
        {
            "type": "header",
            "text": {
                "type": "plain_text",
                "text": ":construction_worker: " + username
            }
        }]
    for ticket in tickets:
        block = {
            "type": "section",
            "fields": [{
                "type": "mrkdwn",
                "text": ":ticket: *" + str(ticket["id"]) + "* "
                },
                {
                    "type": "mrkdwn",
                    "text": ":placard: _" + str(ticket["fields"]["name"]) + "_\n "
                },
                {
                    "type": "mrkdwn",
                    "text": ":information_source: " + str(ticket["fields"]["status"]["name"])
                },
                {
                    "type": "mrkdwn",
                    "text": ":earth_africa: <https://phabricator.wikimedia.org/T" + str(ticket["id"]) + "|T" + str(
                        ticket["id"]) + ">\n\n"
                },
            ],
        }
        blocks.append(block)
        blocks.append({
            "type": "divider"
        })

    return blocks


# Returns a block that represents information stored as a dictionary
def get_blocks_from_dict(dict):
    blocks = get_bot_header()
    for key in dict:
        block = {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": key + " " + dict[key]
            }
        }
        blocks.append(block)
        # blocks.append({
        #     "type": "divider"
        # })

    return blocks


def get_blocks_from_dict_as_fields(dict):
    blocks = get_bot_header()
    block = {
        "type": "section",
        "fields": []
    }
    for key in dict:
        block["fields"].append({
            "type": "mrkdwn",
            "text": key + " " + dict[key]
        })
    blocks.append(block)

    return blocks

# Returns a block that represents information stores as a dictionaries list
def get_blocks_from_dict_list(dict_list):
    blocks = get_bot_header()
    for dict_item in dict_list:
        for key in dict_item:
            block = {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text":  key + " " + dict_item[key]
                }
            }
            blocks.append(block)
        blocks.append({
            "type": "divider"
        })

    return blocks


# Returns a block that represent a message
def get_blocks_from_message(message):
    blocks = get_bot_header()
    block = {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": message
        }
    }
    blocks.append(block)
    return blocks


# Returns a block that represents several messages
def get_blocks_from_messages(messages):
    blocks = get_bot_header()
    for message in messages:
        block = {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": message
            }
        }
        blocks.append(block)
    return blocks


# Returns a block that represent a header with bot information
def get_bot_header():
    return [
        {
            "type": "header",
            "text": {
                "type": "plain_text",
                "text": ":robot_face: PhaBot"
            }
        }]
