import os
import configparser
import message_utils
import constants
import re

from slack_bolt import App
from message_utils import get_blocks_from_message, get_blocks_from_dict, get_blocks_from_dict_list
from phabricator import Phabricator

app = App(
    token=os.environ.get("SLACK_BOT_TOKEN"),
    signing_secret=os.environ.get("SLACK_SIGNIN_SECRET")
)

# Read config file (API_TOKEN and API_BASEPATH for Phabricator API
config = configparser.ConfigParser()
config.read("config.ini")
API_TOKEN = config["Phabricator"]["API_TOKEN"]
API_BASEPATH = config["Phabricator"]["URL"] + "/api"

phabricator = Phabricator(API_TOKEN, API_BASEPATH)


# Event: someone mention bot
@app.event("app_mention")
def event_app_mention(say):
    say("Hello! If you need my help, execute the next command: _/phabot_")


# Command: /my_tickets - Show information about the tickets of the current user
@app.command("/my_tickets")
def my_tickets_command(ack, respond, command):
    ack()
    if command["text"] == "":
        command["text"] = "all"

    match command["text"]:
        case "all":
            username = phabricator.get_current_username()
            my_tickets = phabricator.get_assigned_tickets_by_username(username)
            blocks = message_utils.get_user_tickets_message(username, my_tickets)
            respond(blocks=blocks, text="My tickets")
        case _:
            status = command["text"]
            if status not in ['open', 'close', 'invalid', 'progress']:
                message = ":x: Invalid status"
                respond(blocks=get_blocks_from_message(message), text="My tickets")
                return

            username = phabricator.get_current_username()
            my_tickets = phabricator.get_assigned_tickets_by_username_and_status(username, status)
            if len(my_tickets) == 0:
                message = ":information_source: No tickets for this status"
                respond(blocks=get_blocks_from_message(message), text="My tickets")
                return

            blocks = message_utils.get_user_tickets_message(username, my_tickets)
            respond(blocks=blocks, text="My tickets")


# Command: /ticket - Allows to perform several actions for showing/editing tickets
@app.command("/ticket")
def ticket_command(ack, respond, command):
    ack()
    command_parts = command["text"].split()
    if len(command_parts) < 2:
        respond(blocks=get_blocks_from_message(":x: Invalid arguments count"))
        return

    ticket_id = command_parts[0]
    action = command_parts[1]
    match action:
        case "info":
            ticket_info = phabricator.get_ticket_info(ticket_id)
            blocks = message_utils.get_blocks_from_dict_as_fields(ticket_info)
            respond(blocks=blocks, text="Ticket information")
        case "title":
            ticket_title = phabricator.get_ticket_title(ticket_id)
            message = ":information_source: The ticket _" + ticket_id + "_ is about _" + ticket_title + "_"
            blocks = message_utils.get_blocks_from_message(message)
            respond(blocks=blocks, text="Ticket title information")
        case "description":
            ticket_description = phabricator.get_ticket_description(ticket_id)
            message = ":information_source: The description of the ticket _" + ticket_id + "_ is _" + ticket_description + "_"
            blocks = message_utils.get_blocks_from_message(message)
            respond(blocks=blocks, text="Ticket description information")
        case "comments":
            ticket_comments = phabricator.get_ticket_comments(ticket_id)
            if ticket_comments is None:
                respond(blocks=get_blocks_from_message(":x: I'm sorry!. The ticket *" + ticket_id + "* does not exist"))
                return
            # messages = [":page_facing_up:" + item for item in ticket_comments]
            # respond(blocks=get_blocks_from_messages(messages))
            respond(blocks=get_blocks_from_dict_list(ticket_comments))
        # FIXME new structure for comments. Now it's a dict with info from user and comment
        case "last_comment":
            ticket_comments = phabricator.get_ticket_comments(ticket_id)
            if ticket_comments is None:
                respond(blocks=get_blocks_from_message(":x: I'm sorry. The ticket *" + ticket_id + "* does not exist"))
                return
            respond(blocks=get_blocks_from_dict(ticket_comments[-1]))
        case "add_comment":
            comment = command["text"][command["text"].index("add_comment") + len("add_comment") + 1:]
            phabricator.add_comment_to_ticket(ticket_id, comment)
            respond(blocks=get_blocks_from_message(":gear: A comment has been added to the ticket " + ticket_id))
        case "status":
            ticket_status = phabricator.get_ticket_status(ticket_id)
            respond(
                blocks=get_blocks_from_message(":information_source: The ticket " + ticket_id + " is " + ticket_status))
        case constants.OPEN_STATUS:
            phabricator.open_ticket(ticket_id)
            respond(blocks=get_blocks_from_message(":gear: The ticket " + ticket_id + " has been opened"))
        case constants.IN_PROGRESS_STATUS:
            ticket_status = phabricator.get_ticket_status(ticket_id)
            if ticket_status != constants.OPEN_STATUS:
                message = ":x: Wrong status for this ticket. It's not open"
                respond(blocks=get_blocks_from_message(message))
                return
            phabricator.progress_ticket(ticket_id)
            message = ":gear: The ticket " + ticket_id + " has been set in progress"
            respond(blocks=get_blocks_from_message(message))
        case constants.CLOSE_STATUS:
            phabricator.close_ticket(ticket_id)
            respond(blocks=get_blocks_from_message(":gear: The ticket " + ticket_id + " has been closed"))
        case constants.INVALID_STATUS:
            phabricator.invalidate_ticket(ticket_id)
            respond(blocks=get_blocks_from_message(":gear: The ticket " + ticket_id + " has been marked as invalid"))
        case _:
            respond(blocks=get_blocks_from_message(":x: Invalid argument (execute /phabot for help)"))
            return


@app.command("/create_ticket")
def create_ticket_command(ack, respond, command):
    ack()
    command_parts = command["text"].split("#")
    title = command_parts[0]
    description = command_parts[1]
    # FIXME (feature currently disabled)
    # ticket_id = phabricator.create_task(title, description)
    ticket_id = "sample_ticket_id"
    respond(blocks=message_utils.get_blocks_from_message(":gear: The ticket <https://phabricator.wikimedia.org/T" +
                                                         ticket_id + "|T" + ticket_id + "> has been created (feature currently disabled)"))


# Command: /user - Shows tasks information about a specific user
@app.command("/user")
def user_command(ack, respond, command):
    ack()
    command_parts = command["text"].split()
    if len(command_parts) == 1:
        command_parts.append("all")

    username = command_parts[0]
    status_filter = command_parts[1]
    match status_filter:
        case "all":
            user_tickets = phabricator.get_assigned_tickets_by_username(username)
        case _:
            user_tickets = phabricator.get_assigned_tickets_by_username_and_status(username, status_filter)
    blocks = message_utils.get_user_tickets_message(username, user_tickets)
    respond(blocks=blocks, text="Tickets list for an user")


# Command: /phabot - Shows information about the bot and how it works
@app.command("/phabot")
def phabbot_command(ack, respond, command):
    ack()
    if command["text"] == "":
        command["text"] = "help"

    command_parts = command["text"].split()
    match command_parts[0]:
        case "help":
            help = {":gear: /ticket <ticket_id> info": ":information_source: Show several details of a ticket",
                    ":gear: /ticket <ticket_id> title": ":information_source: Show the ticket title",
                    ":gear: /ticket <ticket_id> description": ":information_source: Show the ticket description",
                    ":gear: /ticket <ticket_id> comments": ":information_source: Show all the title comments",
                    ":gear: /ticket <ticket_id> last_comment": ":information_source: Show the last comment of a ticket",
                    ":gear: /ticket <ticket_id> add_comment <comment>": ":information_source: Add a comment to a ticket",
                    ":gear: /my_tickets <all | open | close | invalid | stalled | progress>":
                        ":information_source: Show your tickets with some details",
                    ":gear: /ticket <ticket_id> status": ":information_source: Check the status of a ticket",
                    ":gear: /ticket <ticket_id> <close | open | invalid | progress>":
                        ":information_source: Change the ticket status to the provided one",
                    ":gear: /create_ticket <title>#<description": ":information_source: Create a new ticket",
                    ":gear: /user <username> <all | open | close | invalid | stalled | progress>":
                        ":information_source: Show information about the tickets from an user"}
            blocks = get_blocks_from_dict(help)
            respond(blocks=blocks, text="Help")
        case _:
            respond(":x: Invalid arguments")


# Event: a message was sent to a channel or directly to the bot
@app.event("message")
def read_message(message, say):
    # We can filter my message_type in the case we enable message.history event subscription
    # messages.im is the event to read messages written directly to the bot
    # messages.channel is the event to read messages wherever the bot is

    # if message["channel_type"] != "im":
    #    return

    message_text = message["text"]
    if message_text == "phabot":
        say("This is me!!")
        return

    # If someone sends a message that contains the pattern TXXXXX (where X is [0-9]),
    # the bot prints some details about the ticket
    ticket_pattern = re.compile("T[0-9]*")
    found = ticket_pattern.search(message_text)
    if found:
        ticket_id = found.group(0)[1:]
        ticket_info = phabricator.get_ticket_info(ticket_id)
        blocks = message_utils.get_blocks_from_dict_as_fields(ticket_info)
        say(blocks=blocks, text="Ticket info")
        # say("Do you need help with some ticket? execute _/phabot_ and you'll see all I can do for you")
        return

    # If someone sends a message that contains the pattern 'ticket XXXXX' (where X is [0-9]),
    # the bot prints some details about the ticket
    ticket_pattern = re.compile("ticket [0-9]*")
    found = ticket_pattern.search(message_text)
    if found:
        ticket_id = found.group(0).split(" ")[1]
        ticket_info = phabricator.get_ticket_info(ticket_id)
        blocks = message_utils.get_blocks_from_dict_as_fields(ticket_info)
        say(blocks=blocks, text="Ticket info")
        return


if __name__ == "__main__":
    app.start(port=int(os.environ.get("SLACK_PORT", 3000)))
