# Ticket statuses
OPEN_STATUS = "open"
IN_PROGRESS_STATUS = "progress"
CLOSE_STATUS = "closed"
INVALID_STATUS = "invalid"
STALLED_STATUS = "stalled"

# Ticket info parts
TICKET_ID = ":ticket:"
TICKET_OWNER = ":construction_worker:"
TICKET_TITLE = ":placard:"
TICKET_DESCRIPTION = ":page_facing_up:"
TICKET_STATUS_NAME = ":information_source:"
TICKET_LINK = ":earth_africa:"
