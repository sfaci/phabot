# [WIP] phabot

Phabot is a Slack bot to integrate Slack with Phabricator created during the Code Jam celebrated in March 2023

## Features

* Show some details of all your tickets (ticket_id, title, description, current status and a link to the ticket in Phabricator). The list can be filtered by the ticket status (_open_, _close_, _invalid_, _stalled_ or _progress_)
* Show some details of all the tickets from a specific user (ticket_id, title, description, current status and a link to the ticket in Phabricator). The list can be filtered by the ticket status (_open_, _close_, _invalid_, _stalled_ or _progress_)
* Show some details of a specific ticket: a summary, title, description, all comments or the last comment
* Check the current status for a specific ticket
* Change the current status of any ticket (_open_, _close_, _invalid_, _progress_)
* Add a comment to a ticket
* Create a new ticket with a title and a description

## Requirements

* Python 3
* Some python libraries
  * slack_bolt
  * requests
  * configparser
  * re
* To develop locally
  * ngrok

## Configuration

* Create a Slack application in [Slack Apps](https://api.slack.com/apps/)
  * Add Bot Token Scopes (OAuth & Permissions): _app\_mention:read_, _channels:history_, _chat:write_, _commands_ and _im:history_
  * Enable Event Subscriptions and add the Request URL (you can get a public exposed URL using [ngrok](https://ngrok.com/)). The Request URL will be https://YOUR_PUBLIC_URL_HERE/slack/events
    * Subscribe to the following bot events: _app\_mention_, _message.channels_ and _message.im_
  * Create Slash Commands:
    * You have to register all the available commands: _/my_tickets_, _/ticket_, _/user_, _/phabot_ and _/create_ticket_ with some details:
      * Command: The command name (starting with '/')
      * Request URL: The same URL you have written as Request URL to enable Event Subscriptions
      * Short Description: A short description for this command
      * Usage Hint: A hint about how to use it
  * Install the application into a Workspace
* Set up your Python application:
  * config.ini.sample: Put here your Phabricator API KEY and rename to _config.ini_
  * Makefile.sample: Put here your Slack bot token (available in the _OAuth & Permissions_ section of your app) and Slack Signing Key (available in the _Basic Information_ section) and rename to _Makefile_

## How to run locally

First, you need to run a local HTTP server to create a public URL to expose our bot to Slack (this URL is the one you 
have set to enable Event Subscriptions and Slack Commands (see __Configuration__ section):

```bash
user@localhost:$ ngrok http 3000
```

Now, we can run the bot as a Python application:

```bash
user@localhost:$ source .venv/bin/activate
user@localhost:$ make run
```

## Usage

You can type _/phabot_ directly in any Slack channel (inside the Workspace where the bot is installed) to see all the commands and parameters you can execute